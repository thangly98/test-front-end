import React from 'react'
import { Route, Switch } from 'react-router'
import HomePage from '../page/home'

const Routing = () => (
    <Switch>
        <Route path='/' exact component={() => <HomePage />} />
    </Switch>
)

export default Routing
